'use strict';

/**
 * @ngdoc function
 * @name swimmingApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the swimmingApp
 */
angular.module('swimmingApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
