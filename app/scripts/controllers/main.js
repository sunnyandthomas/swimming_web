'use strict';

/**
 * @ngdoc function
 * @name swimmingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the swimmingApp
 */
angular.module('swimmingApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
