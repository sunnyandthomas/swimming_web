'use strict';

/**
 * @ngdoc overview
 * @name swimmingApp
 * @description
 * # swimmingApp
 *
 * Main module of the application.
 */
angular
  .module('swimmingApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $locationProvider) { 
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .otherwise({
        redirectTo: '/'
      }); 
	  
	 $locationProvider.html5Mode(true);

  });
